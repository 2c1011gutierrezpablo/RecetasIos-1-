//
//  Receta.h
//  RecetasIoss
//
//  Created by pablo Gutierrez on 1/2/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Receta : NSObject

@property NSString * titulo;// nombre de la receta
@property NSString * descripcion;//descripcion de lo que va a conistir la receta
@property NSArray * pasos; // pasos a seguir en la receta
@property NSMutableArray * ingredientesDeLaReceta; //ingredientes que lleva la receta
@property NSString *urlImagen;

-(id)initReceta:(NSString*)titulo ydescripcion:(NSString*)descripcion ypasos:(NSArray*)pasos yingredientes:(NSMutableArray*)listaIngredientes yUrlImagen:(NSString*)urlImagen;
-(NSString*)getIngredientesString;
@end
