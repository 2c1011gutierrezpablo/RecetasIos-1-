//
//  Pasos.m
//  RecetasIoss
//
//  Created by Javi on 6/3/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import "Pasos.h"

@implementation Pasos
-(id)initConDescripcion:(NSString*)descripcion yUrlImagen:(NSString*)urlImagen{
    self = [super init];
    if (self) {
        self.descripcion = descripcion;
        self.urlImagen = urlImagen;
        return self;
    }
    return nil;
    
}
@end
