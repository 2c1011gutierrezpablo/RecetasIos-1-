//
//  Recetas.h
//  RecetasIoss
//
//  Created by pablo Gutierrez on 1/2/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Receta.h"


@interface ListaDeRecetas : NSObject

@property NSMutableArray * ListaDeRecetas;//es una lista con todas las recetas de las que se dispone
-(id)initRecetas;
- (NSUInteger)longitudLista ;
-(void) addReceta:(Receta*)ingredinte;
-(Receta*)mostrarReceta: (NSUInteger)posicion;

-(void)addRecetaConingrediente:(NSString*)idIngredientes;
@end
