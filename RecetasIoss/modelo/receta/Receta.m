//
//  Receta.m
//  RecetasIoss
//
//  Created by pablo Gutierrez on 1/2/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import "Receta.h"
#import "Ingrediente.h"

@implementation Receta

-(id)initReceta:(NSString*)titulo ydescripcion:(NSString*)descripcion ypasos:(NSArray*)pasos yingredientes:(NSMutableArray*)listaIngredientes yUrlImagen:(NSString*)urlImagen{
    
    
    self = [super init];
    if (self) {
        
        self.titulo = titulo;
        self.descripcion = descripcion;
        self.pasos = pasos;
        self.ingredientesDeLaReceta = listaIngredientes;
        self.urlImagen = urlImagen;
        
        return self;
    }
    return nil;
    
}
-(NSString*)getIngredientesString{
    return [self.ingredientesDeLaReceta componentsJoinedByString:@", "];;
}

@end
