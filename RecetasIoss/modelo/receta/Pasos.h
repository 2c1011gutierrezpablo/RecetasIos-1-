//
//  Pasos.h
//  RecetasIoss
//
//  Created by Javi on 6/3/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Pasos : NSObject
@property NSString* descripcion;
@property NSString* urlImagen;

-(id)initConDescripcion:(NSString*)descripcion yUrlImagen:(NSString*)urlImagen;
@end
