//
//  Recetas.m
//  RecetasIoss
//
//  Created by pablo Gutierrez on 1/2/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import "ListaDeRecetas.h"
#import "jsonRead.h"
#import "Receta.h"
#import "Ingrediente.h"
#import "Pasos.h"



@implementation ListaDeRecetas

-(id)initRecetas {
    
    self = [super init];
    if (self) {
        

       
        self.ListaDeRecetas  = [[NSMutableArray alloc] init];
        
        NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"idI"];
        
        //NSString *savedValue = @"1,2,3,4,5,6,7,8";
        if (![savedValue isEqualToString:@""] && savedValue !=nil) {
            [self addRecetaConingrediente:savedValue];
        }
       
        
        
        return self;
    }
    return nil;
    
}

-(void) addReceta:(Receta*)recetaAdd{//añade recetas a la lita
    
    
    [self.ListaDeRecetas addObject:recetaAdd];
    
}


- (NSUInteger)longitudLista {//dice cuantas recetas hay
    return [self.ListaDeRecetas count];
}





-(Receta*)mostrarReceta: (NSUInteger)posicion{//devuelve una receta
    
    Receta* recetaMostrar =[self.ListaDeRecetas objectAtIndex:posicion];
    return recetaMostrar;
}

-(void)addRecetaConingrediente:(NSString*)idIngredietnes{
    
    NSMutableArray * a =  [jsonRead conexionServidor:[@"https://reasype.com/api/recetasDisponiblesConIng.php?ing=" stringByAppendingString:idIngredietnes] ];
    self.ListaDeRecetas = [[NSMutableArray alloc] init];
    
    
    for (int i=0; i<[a count]; i++) {
        NSDictionary *arrayResult = [a objectAtIndex:i];
        
        NSArray *ingArray = [arrayResult objectForKey:@"ingredientes"];
        
        NSMutableArray *listaIngredientesRecetas = [[NSMutableArray alloc]init];
        
        for (int j=0; j<[ingArray count]; j++) {
            NSDictionary *ingResult = [ingArray objectAtIndex:j];
            
            
            Ingrediente * i1= [[Ingrediente alloc] initIngrediente:[ingResult objectForKey:@"nombre"] ycantidad:10.0 ytipo:@"vegetal" yunidad:[ingResult objectForKey:@"unidad"] yfechaDeCaducudad:nil ycalorias:50 yid:[[ingResult objectForKey:@"id"] intValue ]yimagen:[arrayResult objectForKey:@"urlImagen"]  ];
            [listaIngredientesRecetas addObject:i1];
        }
        
        NSArray *pasoArray = [arrayResult objectForKey:@"pasos"];
        
        NSMutableArray *listapasosRecetas = [[NSMutableArray alloc]init];
        
        for (int j=0; j<[pasoArray count]; j++) {
            NSDictionary *pasosResult = [pasoArray objectAtIndex:j];
            Pasos* paso = [[Pasos alloc] initConDescripcion:[pasosResult objectForKey:@"descripcion"] yUrlImagen:[pasosResult objectForKey:@"urlImagen"]];
            [listapasosRecetas addObject:paso];
            
        }
        
        
        
        Receta* r = [[Receta alloc] initReceta:[arrayResult objectForKey:@"nombre"] ydescripcion:[arrayResult objectForKey:@"descripcion"] ypasos:listapasosRecetas yingredientes:listaIngredientesRecetas yUrlImagen:[arrayResult objectForKey:@"urlImagen"]];
        
        
        [self.ListaDeRecetas addObject:r];
        
    }
}

@end
