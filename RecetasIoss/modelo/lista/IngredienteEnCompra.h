//
//  IngredienteEnCompra.h
//  RecetasIoss
//
//  Created by pablo Gutierrez on 15/2/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Ingrediente.h"

@interface IngredienteEnCompra : NSObject

@property NSMutableArray * listaDeIngredientes;

-(id)initIngredientes;
- (NSUInteger)longitudLista ;
-(void) addIngredinte:(Ingrediente*)ingredinte;
-(Ingrediente*)mostrarReceta: (NSUInteger)posicion;
-(BOOL)contieneEsteObjeto:(Ingrediente*)ingrediente;

@end
