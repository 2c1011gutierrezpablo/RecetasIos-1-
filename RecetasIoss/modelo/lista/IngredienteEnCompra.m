//
//  IngredienteEnCompra.m
//  RecetasIoss
//
//  Created by pablo Gutierrez on 15/2/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import "IngredienteEnCompra.h"
#import "jsonRead.h"

@implementation IngredienteEnCompra

-(id)initIngredientes {
    
    self = [super init];
    if (self) {
        
       
            self.listaDeIngredientes = [[NSMutableArray alloc] init];
        
        return self;
    }
    return nil;
    
}

-(void) addIngredinte:(Ingrediente*)ingredinte{//añade un ingrediente
    
    
    [self.listaDeIngredientes addObject:ingredinte];
    
}


- (NSUInteger)longitudLista {//dice cauntos ingredientes hay
    return [self.listaDeIngredientes count];
}

-(id)init {
    if (self = [super init]) {
        [self listaDeIngredientes];
        return self;
    }
    return nil;
    
}



-(Ingrediente*)mostrarReceta: (NSUInteger)posicion{//devuelve un ingrediente
    
    Ingrediente* ingrediente =[self.listaDeIngredientes objectAtIndex:posicion];
    return ingrediente;
}

-(BOOL)contieneEsteObjeto:(Ingrediente*)ingrediente{
    
    for (Ingrediente * i in self.listaDeIngredientes) {
        
        NSRange r = [i.nombre rangeOfString:ingrediente.nombre options:NSCaseInsensitiveSearch];
        
        if ( r.location != NSNotFound) {
            
            return YES;
        }
    }
    
    return NO;
}



@end
