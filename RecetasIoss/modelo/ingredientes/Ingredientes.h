//
//  Ingredientes.h
//  RecetasIoss
//
//  Created by pablo Gutierrez on 29/1/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Ingrediente.h"

@interface Ingredientes : NSObject

@property NSMutableArray * ListaIngredientes;//es una lista con todos los ingredientes que se dispone
-(id)initIngredientes:(int)idActividad;
- (NSUInteger)longitudLista ;
-(void) addIngredinte:(Ingrediente*)ingredinte;
-(Ingrediente*)mostrarReceta: (NSUInteger)posicion;
-(BOOL)contieneEsteObjeto:(Ingrediente*)ingrediente;
-(void)borraObjeto:(NSString *)i;

@end
