//
//  Ingredientes.m
//  RecetasIoss
//
//  Created by pablo Gutierrez on 29/1/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import "Ingredientes.h"
#import "jsonRead.h"
@class Ingrediente;

@implementation Ingredientes

-(id)initIngredientes:(int)idActividad {
    
    self = [super init];
    if (self) {
        
        self.ListaIngredientes = [[NSMutableArray alloc] init];
        //peticon para recuperar los ingrdeientes del servidor 
        if (idActividad==1) {
           
        
        
       NSMutableArray * a =  [jsonRead conexionServidor:@"https://reasype.com/api/ingredientes.php?id=1,2,3,4,5,6,7,8"];
            self.ListaIngredientes = [[NSMutableArray alloc] init];
            
            
            for (int i=0; i<[a count]; i++) {
                NSDictionary *arrayResult = [a objectAtIndex:i];
                
                
                Ingrediente * i1= [[Ingrediente alloc] initIngrediente:[arrayResult objectForKey:@"nombre"] ycantidad:10.0 ytipo:@"vegetal" yunidad:[arrayResult objectForKey:@"unidad"] yfechaDeCaducudad:nil ycalorias:50 yid:[[arrayResult objectForKey:@"id"] intValue ] yimagen:[arrayResult objectForKey:@"urlImagen"] ];
                [self.ListaIngredientes addObject:i1];
                
            }
        }else if (idActividad==2){
            
            NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"idIC"];
            
            NSMutableArray * a =  [jsonRead conexionServidor:[@"https://reasype.com/api/ingredientes.php?id=" stringByAppendingString:savedValue]];
            self.ListaIngredientes = [[NSMutableArray alloc] init];
            
            
            for (int i=0; i<[a count]; i++) {
                NSDictionary *arrayResult = [a objectAtIndex:i];
                
                
                Ingrediente * i1= [[Ingrediente alloc] initIngrediente:[arrayResult objectForKey:@"nombre"] ycantidad:10.0 ytipo:@"vegetal" yunidad:[arrayResult objectForKey:@"unidad"] yfechaDeCaducudad:nil ycalorias:50 yid:[[arrayResult objectForKey:@"id"] intValue ] yimagen:[arrayResult objectForKey:@"urlImagen"] ];
                [self.ListaIngredientes addObject:i1];
                
            }
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"idIC"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        
        return self;
    }
    return nil;
    
}

-(void) addIngredinte:(Ingrediente*)ingredinte{//añade un ingrediente
    
    
    [self.ListaIngredientes addObject:ingredinte];
    
}


- (NSUInteger)longitudLista {//dice cauntos ingredientes hay
    return [self.ListaIngredientes count];
}

-(id)init {
    if (self = [super init]) {
        [self ListaIngredientes];
        return self;
    }
    return nil;
    
}



-(Ingrediente*)mostrarReceta: (NSUInteger)posicion{//devuelve un ingrediente
    
    Ingrediente* ingrediente =[self.ListaIngredientes objectAtIndex:posicion];
    return ingrediente;
}

-(BOOL)contieneEsteObjeto:(Ingrediente*)ingrediente{
    
    for (Ingrediente * i in self.ListaIngredientes) {
        
        NSRange r = [i.nombre rangeOfString:ingrediente.nombre options:NSCaseInsensitiveSearch];
        
        if ( r.location != NSNotFound) {
            
            return YES;
        }
    }
    
    return NO;
}

-(void)borraObjeto:(NSString *)i{
    
    NSString * i2 = i;
    for (NSInteger i = self.ListaIngredientes.count - 1; i >= 0 ; i--) {
        
        if ([[self.ListaIngredientes objectAtIndex:i] idIN] == [i2 intValue]) {
            [self.ListaIngredientes removeObjectAtIndex:i];
        }
        
        
    }
    
}

@end
