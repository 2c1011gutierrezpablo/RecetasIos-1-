//
//  Ingrediente.h
//  RecetasIoss
//
//  Created by pablo Gutierrez on 29/1/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Ingrediente : NSObject

@property NSString * nombre;//nombre del ingrediente
@property double cantidad;//cantidad ingrediente que se dispone
@property NSString * tipo; // tipo de ingredinte verdura por ej
@property NSString * unidad;//unidad de medida del elemento ej kg
@property NSDate * fechaDeCaducidad;// dia en el que el ingrediente no esta apto para el consumo
@property int calorias;
@property int idIN;
@property NSString * img;

-(id)initIngrediente:(NSString* )nombe ycantidad:(double)cantidad ytipo:(NSString*)tipo yunidad:(NSString*)unidad yfechaDeCaducudad:(NSDate*)fechaDeCaducudad ycalorias:(int)calorias yid:(int)idIN yimagen:(NSString*)img;

@end
