//
//  Ingrediente.m
//  RecetasIoss
//
//  Created by pablo Gutierrez on 29/1/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import "Ingrediente.h"

@implementation Ingrediente


-(id)initIngrediente:(NSString* )nombe ycantidad:(double)cantidad ytipo:(NSString*)tipo yunidad:(NSString*)unidad yfechaDeCaducudad:(NSDate*)fechaDeCaducudad ycalorias:(int)calorias yid:(int)idIN
yimagen:(NSString*)img{
    
    self = [super init];
    if (self) {
        self.idIN = idIN;
        self.nombre = nombe;
        self.cantidad = cantidad;
        self.tipo = tipo;
        self.unidad = unidad;
        self.fechaDeCaducidad = fechaDeCaducudad;
        self.calorias = calorias;
        self.img = img;
        
        return self;
    }
    return nil;
    
    
}

- (NSString *)description {
    return self.nombre;
}




@end
