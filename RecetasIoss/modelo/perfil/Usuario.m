//
//  Usuario.m
//  RecetasIoss
//
//  Created by pablo Gutierrez on 25/2/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import "Usuario.h"

@implementation Usuario

-(id)initUser:(NSString*)nombre ycorreo:(NSString*)correo ycontasena:(NSString*)contrasena yidN:(int)idN{
    
    self = [super init];
    if (self) {
        
        self.nombre = nombre;
        self.correo = correo;
        self.contrasena = contrasena;
        self.idN = idN;
        
        }
    return self;
}

@end
