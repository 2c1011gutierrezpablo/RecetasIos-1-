//
//  Usuario.h
//  RecetasIoss
//
//  Created by pablo Gutierrez on 25/2/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Usuario : NSObject

@property NSString * nombre;
@property NSString * correo;
@property NSString * contrasena;
@property int  idN;

-(id)initUser:(NSString*)nombre ycorreo:(NSString*)correo ycontasena:(NSString*)contrasena yidN:(int)idN;

@end
