
//
//  Post.m
//  RecetasIoss
//
//  Created by pablo Gutierrez on 5/3/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import "Post.h"

@implementation Post

+(NSString*)sedPost:(NSString*)url yparam:(NSString*)param ytipo:(NSString*)tipo{
    
    __block NSString * responseText =@"";
    
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration];
    
    // Setup the request with URL
    NSURL *url2 = [NSURL URLWithString:url];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url2];
    
    // Convert POST string parameters to data using UTF8 Encoding
    NSString *postParams = param;
    NSData *postData = [postParams dataUsingEncoding:NSUTF8StringEncoding];
    
    // Convert POST string parameters to data using UTF8 Encoding
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setHTTPBody:postData];
    
    
    
    NSURLSessionDataTask *task = [defaultSession dataTaskWithRequest:urlRequest completionHandler:^( NSData *data, NSURLResponse *response, NSError *error )
                                  {
                                      dispatch_async( dispatch_get_main_queue(),
                                                     ^{
                                                         // parse returned data
                                                         NSString *result = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                                                         
                                                         NSLog( @"%@", result );
                                                     } );
                                  }];
    
    [task resume];
    
    return responseText;
}

+(void)sendPostReceta:(Receta*)r{

    
        NSString * idReceta =[self sedPost:@"https://reasype.com/recetas" yparam:[self generateReceta:r] ytipo:@""];
    
            
            for (int i = 0 ; i< r.pasos.count; i++){
                
                [self sedPost:@"https://reasype.com/pasos" yparam:[self generatePasos:@"50" ypasos:[r.pasos objectAtIndex:i] ydes:r.descripcion] ytipo:@""];
                
            }
            
            for (int i = 0 ; i< r.ingredientesDeLaReceta.count; i++){
                
                [self sedPost:@"https://reasype.com/ingredientes_cantidad_recetas" yparam:[self generateIngredientes:[r.ingredientesDeLaReceta objectAtIndex:i] yideRe:@"50"] ytipo:@""];
                
                
            }
           

}

// devuelve la cavecera para hacer añadir receta
+(NSString*)generateReceta:(Receta *)r{
    
    NSString * receta =[NSString stringWithFormat:@"&nombre=%@&descripcion=%@&urlImagen=%@",r.titulo,r.descripcion,[self generateImagen]];
    
    return receta;
}

//añande la cabecera para añadir los pasos
+(NSString*)generatePasos:(NSString*)idReceta ypasos:(NSString*)idPaso ydes:(NSString*)des{
    
    NSString * receta =[NSString stringWithFormat:@"&id_receta=%@&id_paso=%@&descripcion=%@&urlImagen=%@",idReceta,idPaso,des,@""];
    
    return receta;
    
}


+(NSString*)generateImagen{
    
    return nil;
}

//añande la cabecera para añadir los ingredientes
+(NSString*)generateIngredientes:(Ingrediente*)i yideRe:(NSString*)ideRe{
    
    NSString * receta =[NSString stringWithFormat:@"&id_receta=%@&id_ingrediente=%d&cantidad=%f",ideRe,i.idIN,i.cantidad];
    
    return receta;
    
}





@end
