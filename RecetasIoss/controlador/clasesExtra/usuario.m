//
//  usuario.m
//  RecetasIoss
//
//  Created by pablo Gutierrez on 22/2/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import "usuario.h"

@implementation usuario

-(id)initUser:(NSString *)nombre ycorreo:(NSString *)correo yimagen:(NSString *)imagen ycontrasena:(NSString *)contrasena{
    
    
    self = [super init];
    if (self) {
        
        self.nombre = nombre;
        self.correoElectronico = correo;
        self.image = imagen;
        self.contrasena = contrasena;
        
        return self;
    }
    return nil;
    
}

@end
