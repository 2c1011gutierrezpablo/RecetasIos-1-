//
//  usuario.h
//  RecetasIoss
//
//  Created by pablo Gutierrez on 22/2/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface usuario : NSObject

@property NSString * nombre;
@property NSString * correoElectronico;
@property NSString * image;
@property NSString * contrasena;

-(id)initUser:(NSString *)nombre ycorreo:(NSString *)correo yimagen:(NSString *)imagen ycontrasena:(NSString *)contrasena;

@end
