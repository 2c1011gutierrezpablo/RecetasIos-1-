//
//  botonEspecial.h
//  RecetasIoss
//
//  Created by pablo Gutierrez on 21/2/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface botonEspecial : UIButton

//es un boton igual que todos pero sipone de una propiedad tipo int la cual nos permite poder pasar en el la posicion del ingredietne para poder modificar su cantidad
@property int posicion;

@end
