//
//  Post.h
//  RecetasIoss
//
//  Created by pablo Gutierrez on 5/3/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Receta.h"
#import "Ingrediente.h"

@interface Post : NSObject

+(NSString*)sedPost:(NSString*)url yparam:(NSString*)param ytipo:(NSString*)tipo;
+(void)sendPostReceta:(Receta*)r;

@end
