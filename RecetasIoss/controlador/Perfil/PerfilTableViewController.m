//
//  PerfilTableViewController.m
//  RecetasIoss
//
//  Created by pablo Gutierrez on 25/2/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import "PerfilTableViewController.h"
#import "usuario.h"
#import "inicioSesionViewController.h"



@interface PerfilTableViewController ()
@property (weak, nonatomic) IBOutlet UILabel *etiquetaEmile;// se mostrara el emile asociado
@property (weak, nonatomic) IBOutlet UILabel *etiquetaUser;//se mostra el nombre del user

@property Usuario * user;//usuario logeado

@end

@implementation PerfilTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 2;
}





- (IBAction)prepareForUnwin:(UIStoryboardSegue *)segue
{
    
    //al hacer el login se hara :
    if ([[segue identifier] isEqualToString:@"cargarUser"]){
        
        inicioSesionViewController * inicio = (inicioSesionViewController*)segue.sourceViewController;
        
        //se asignara el user logeado
        
        self.user = inicio.nuevoUser ;
        
        //se cerrara la view
        
        [self dismissViewControllerAnimated:YES completion:Nil];
       
        //se mostrara la infro del user
        
        self.etiquetaUser.text = self.user.nombre;
        self.etiquetaEmile.text = self.user.correo;
        
    }
}

@end
