//
//  inicioSesionViewController.m
//  RecetasIoss
//
//  Created by pablo Gutierrez on 2/3/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import "inicioSesionViewController.h"
#import "jsonRead.h"
//http://reasype.com/api/autenticaUsuario.php?usuario=javi&pass=alumnoMac

@interface inicioSesionViewController ()
@property (weak, nonatomic) IBOutlet UITextField *usuario;//valor del user
@property (weak, nonatomic) IBOutlet UITextField *password;//valor de lacontraseña

- (IBAction)inicioSesion:(id)sender;

@end

@implementation inicioSesionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)inicioSesion:(id)sender{
    
    NSString *user = self.usuario.text;
    NSString *pasword = self.password.text;
    
    if (![user isEqualToString:@""] && ![pasword isEqualToString:@""]) {
        
        //si no hay ningun campo vacion se hara una peticion al servidor para logear al usaurio
        
        NSMutableArray * a =  [jsonRead conexionServidor:[NSString stringWithFormat:@"https://reasype.com/api/autenticaUsuario.php?usuario=%@&pass=%@",user,pasword] ];
        NSDictionary *arrayResult2 = [a objectAtIndex:0];
        
        //si da uno quiere decir que el usuario existe por lo que se precedera a logearlo 
        if ([(NSString*)[arrayResult2 objectForKey:@"existe"] isEqualToString:@"1"]) {
            self.nuevoUser = [[Usuario alloc] initUser:user ycorreo:@"quieresUnAbrazo@soyelmejor.es" ycontasena:pasword yidN:1];
            
            [self performSegueWithIdentifier:@"cargarUser" sender:self];
            
        }else{
            NSLog(@"no existe");
            
        }
        
        
    }
    
}


@end
