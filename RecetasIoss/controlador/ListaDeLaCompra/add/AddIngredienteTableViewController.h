//
//  AddIngredienteTableViewController.h
//  RecetasIoss
//
//  Created by pablo Gutierrez on 9/2/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Ingrediente.h"

@interface AddIngredienteTableViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource>

@property NSMutableArray * listaIngrediente;
@property NSMutableArray * ingredientesAdd;


@property (strong, nonatomic) id someProperty;



@end
