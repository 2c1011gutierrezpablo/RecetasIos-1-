//
//  AddIngredienteTableViewController.m
//  RecetasIoss
//
//  Created by pablo Gutierrez on 9/2/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import "AddIngredienteTableViewController.h"
#import "RecetasIoss-Swift.h"
#import "ApiCodigoDeBarras.h"
#import "botonEspecial.h"
#import "Ingrediente.h"
#import "Ingredientes.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "jsonRead.h"
#import "IngredienteTableViewCell.h"

@interface AddIngredienteTableViewController (){
    BOOL shouldBeginEditing;
    UITapGestureRecognizer *tap;
}

@property NSMutableArray * listaDeBusqueda;
@property NSString * codigo;
@property (weak, nonatomic) IBOutlet UISearchBar *barraDeBusqueda;

#warning comentar esta clase

@end

@implementation AddIngredienteTableViewController

{
    int  numeroDeIngredientes;
    Ingredientes * claseIngre;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.ingredientesAdd = [[NSMutableArray alloc] init];
    
   
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        claseIngre = [[Ingredientes alloc] initIngredientes:1];
        
        self.listaIngrediente = claseIngre.ListaIngredientes;
        
        //self.resultdoBusqueda = [[NSMutableArray alloc] init];
        
        numeroDeIngredientes =(int) self.listaIngrediente.count;
        
        shouldBeginEditing = YES;
        
        tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            
            [self.tableView reloadData];
        });
    });
    
    self.tableView.allowsMultipleSelection = true;
    // Uncomment the following line to preserve selection between presentations.
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return numeroDeIngredientes;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    IngredienteTableViewCell *celda = [tableView dequeueReusableCellWithIdentifier:@"celda1" forIndexPath:indexPath];

    botonEspecial *buttonCantidad = (botonEspecial *)[celda viewWithTag:104];
    
    buttonCantidad.posicion = (int)indexPath.row;
    
    [buttonCantidad addTarget:self action:@selector(abreDialogo:) forControlEvents:UIControlEventTouchUpInside];
    
    Ingrediente * ingredinete = [[Ingrediente alloc] init];
    
    // Configure the cell...
    if([[tableView indexPathsForSelectedRows] containsObject:indexPath]) {
        
             ingredinete = [self.listaIngrediente objectAtIndex:indexPath.row];
       
    } else {
      
             ingredinete = [self.listaIngrediente objectAtIndex:indexPath.row];
        
    }
   
    
    [[celda tituloIng]setText:ingredinete.nombre];
    [buttonCantidad  setTitle:[NSString stringWithFormat:@"%.2f%@",ingredinete.cantidad,ingredinete.unidad] forState:UIControlStateNormal];
    celda.accessoryType = UITableViewCellAccessoryNone;
    
    [celda.imagenIng sd_setImageWithURL:[NSURL URLWithString:[@"https://www.reasype.com" stringByAppendingString:ingredinete.img] ] placeholderImage:[UIImage imageNamed:@"exito"]];
    
    return celda;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
   
    
    [self.ingredientesAdd addObject:[self.listaIngrediente objectAtIndex:(NSUInteger)indexPath.row]];
    
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryNone;
   [self.ingredientesAdd removeObject:[self.listaIngrediente objectAtIndex:(NSUInteger)indexPath.row]];
    
}





- (IBAction)Guardarcodigo:(UIStoryboardSegue *)segue
{
    if ([[segue identifier] isEqualToString:@"cerrarLector"]){
        
       
        //self.codigo = vc.codigo;
       
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        self.codigo= (NSString*)[prefs stringForKey:@"Key"];
        //NSLog(@"%@ pasa macho",self.codigo);
        self.barraDeBusqueda.text =self.codigo;
        NSLog(@"%@",self.codigo);
        //[prefs setValue:@"" forKey:@"Key"];
        
        if (![(NSString*)[prefs stringForKey:@"Key"] isEqualToString:@""]) {
             [self anadeIngredienteNuevo];
        }
        
       
        [prefs setValue:@"" forKey:@"Key"];
        
    }if ([[segue identifier] isEqualToString:@"cerrarLector2"]){
        
         NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setValue:@"" forKey:@"Key"];
        
    }
}

-(void)anadeIngredienteNuevo{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSMutableDictionary * ingredienteNUevo = (NSMutableDictionary*) [ApiCodigoDeBarras consigueIngrediente:self.codigo] ;
        
        
        NSMutableDictionary *arrayResult= [ingredienteNUevo objectForKey:@"product"];
        
        NSString * nombreAlimneto = [[arrayResult objectForKey:@"product_name"]  stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        
        NSMutableArray * a =  [jsonRead conexionServidor:[@"https://reasype.com/api/ingredientesConNombreSimilar.php?nombre=" stringByAppendingString:nombreAlimneto]];
        NSDictionary *arrayResult2 = [a objectAtIndex:0];
        
        if ([[arrayResult2 objectForKey:@"porcentaje"] doubleValue]>0) {
            
            
            Ingrediente * i1= [[Ingrediente alloc] initIngrediente:[arrayResult2 objectForKey:@"nombre"] ycantidad:10.0 ytipo:@"vegetal" yunidad:[arrayResult2 objectForKey:@"unidad"] yfechaDeCaducudad:nil ycalorias:50 yid:[[arrayResult2 objectForKey:@"id"] intValue ] yimagen:[arrayResult2 objectForKey:@"urlImagen"] ];
            
            [self.listaIngrediente addObject:i1];
            
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:i1.nombre
                                                                           message:@"has been added after the scan"
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
        
        
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            // This will be called on the main thread, so that
            // you can update the UI, for example.
            [self.tableView reloadData];
            
        });
    });

    
}


-(void)abreDialogo:(UIButton *)sender{
    //conseguimos el indexPath
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    //creamos alert con 2 acciones
    UIAlertController *alert= [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"New qty in %@",[[self.listaIngrediente objectAtIndex:indexPath.row] unidad]] message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        UITextField *textField = alert.textFields[0];
        [(Ingrediente*) [self.listaIngrediente objectAtIndex:indexPath.row] setCantidad:[textField.text integerValue] ];
        [[self tableView] reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    //añadimos las acciones
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = [NSString stringWithFormat:@"Qty in %@",[[self.listaIngrediente objectAtIndex:indexPath.row] unidad]];
        textField.keyboardType = UIKeyboardTypeDefault;
    }];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{//metodo encargado de la busqueda en la searchbar
    
    if(![searchBar isFirstResponder]) {
        // user tapped the 'clear' button
        shouldBeginEditing = NO;
        // do whatever I want to happen when the user clears the search...
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // No explicit autorelease pool needed here.
        // The code runs in background, not strangling
        // the main run loop.
        if ([searchText length]==0) {//si la cadena no tiene nada se carga la lista principal
            
            [self.listaDeBusqueda removeAllObjects];
            [self.listaDeBusqueda addObjectsFromArray:self.listaIngrediente];
        }else{//si tiene algo se cargan todos los elemntos que coincidan con lo ecrito
            
            [self.listaDeBusqueda removeAllObjects];
            for (Ingrediente *i in self.listaIngrediente) {
                
                NSRange r = [i.nombre rangeOfString:searchText options:NSCaseInsensitiveSearch];
                
                if ( r.location != NSNotFound) {
                    
                    [self.listaDeBusqueda addObject:i];
                }
            }
        }
        dispatch_sync(dispatch_get_main_queue(), ^{
            // This will be called on the main thread, so that
            // you can update the UI, for example.
            [self.tableView reloadData];
        });
    });
    
    
    
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{//indica lo que hacer cuando se clica en el boton de busqueda del teclado
    [searchBar resignFirstResponder];
}
-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    self.tableView.allowsSelection = NO;
    [self.view addGestureRecognizer:tap];
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    self.tableView.allowsSelection = YES;
    [self.view removeGestureRecognizer:tap];
    [searchBar resignFirstResponder];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)bar {
    BOOL boolToReturn = shouldBeginEditing;
    shouldBeginEditing = YES;
    return boolToReturn;
}

-(void)dismissKeyboard {
    [self.barraDeBusqueda resignFirstResponder];
}

@end
