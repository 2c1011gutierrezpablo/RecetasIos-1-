//
//  ListaDeLaCompraTableViewController.h
//  RecetasIoss
//
//  Created by pablo Gutierrez on 28/1/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ListaDeLaCompraTableViewController : UITableViewController <UITableViewDataSource,UISearchBarDelegate, UITableViewDelegate>



@end
