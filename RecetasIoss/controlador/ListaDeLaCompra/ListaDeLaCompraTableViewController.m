//
//  ListaDeLaCompraTableViewController.m
//  RecetasIoss
//
//  Created by pablo Gutierrez on 28/1/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import "ListaDeLaCompraTableViewController.h"
#import "IngredienteEnCompra.h"
#import "DetalleDespensaTableViewController.h"
#import "AddIngredienteTableViewController.h"
#import "botonEspecial.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "IngredienteTableViewCell.h"


@interface ListaDeLaCompraTableViewController () {
    BOOL shouldBeginEditing;
    UITapGestureRecognizer *tap;
}

@property IngredienteEnCompra * listaIngredientes;//es la clase que contiene todso los ingredientes
@property(strong,nonatomic) NSMutableArray *resultdoBusqueda;//almacena la busqueda
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property UIImageView *fondo;
- (IBAction)accionBotonAdd:(id)sender;

@end

@implementation ListaDeLaCompraTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
    self.fondo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fondoLista"]];
    self.fondo.contentMode = UIViewContentModeScaleAspectFill;
    
    
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
        //se cargaran los ingredientes de forma sicrona
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        self.listaIngredientes = [[IngredienteEnCompra alloc] initIngredientes];
        self.resultdoBusqueda = [[NSMutableArray alloc] init];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            
            [self.tableView reloadData];
        });
    });
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([self.listaIngredientes longitudLista]>0) {
        self.navigationItem.largeTitleDisplayMode = UINavigationItemLargeTitleDisplayModeAutomatic;
        self.navigationItem.leftBarButtonItem.enabled = YES;
        self.searchBar.hidden = NO;
        [self.tableView willRemoveSubview:self.fondo];
        self.tableView.backgroundView = nil;
        [self.tableView setSeparatorColor:[UIColor lightGrayColor]];

    } else {
        self.navigationItem.largeTitleDisplayMode = UINavigationItemLargeTitleDisplayModeNever;
        self.navigationItem.leftBarButtonItem.enabled = NO;
        self.searchBar.hidden = YES;
        [self.tableView addSubview:self.fondo];
        [self.tableView setSeparatorColor:[UIColor clearColor]];
        self.tableView.backgroundView = self.fondo;
        self.tableView.backgroundView.contentMode = UIViewContentModeCenter;
        [self setEditing:NO];
        [self.tableView setEditing:NO animated:YES];
    }
    if ([self.resultdoBusqueda count]>0) {// si no tiene nada la lista de busqueda se crean tantas celdas como la lista principal tenga
        return [self.resultdoBusqueda count];
    } else {
        return [self.listaIngredientes longitudLista];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    IngredienteTableViewCell *celdaId = [tableView dequeueReusableCellWithIdentifier:@"celdaLista" forIndexPath:indexPath];
    
    if (celdaId == nil) {
        celdaId = [[IngredienteTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"celdaLista"];
    }
    
    
    botonEspecial *buttonCantidad = (botonEspecial *)[celdaId viewWithTag:100];//creamos un boton especial el cual tendra una propiedad
    buttonCantidad.posicion = (int)indexPath.row;//le asignamos la posicion de la celda
    [buttonCantidad addTarget:self action:@selector(abreDialogo:) forControlEvents:UIControlEventTouchUpInside];//le añadimos al boton el metodo a ejecutar cuando se pulse
    Ingrediente * ingredinete = nil;//iniciamos el ingrediente a mostrar
    
  
    if ([self.resultdoBusqueda count]>0) {//si la cadena de busqueda no tiene nada se carga la lista de serie sino la de busqueda
        
        ingredinete = [self.resultdoBusqueda objectAtIndex:indexPath.row];//sacamos el ingrediente de la busqueda
        
    }else{
        
        ingredinete = [self.listaIngredientes mostrarReceta:indexPath.row];//sacamos el ingrdiente de la lista global
        
    }
    [[celdaId tituloIng]setText:ingredinete.nombre];//asignamos a la celda el nombre del ingrdiente
    //ponemos el valor de la cantidad al boton especial
    [buttonCantidad  setTitle:[NSString stringWithFormat:@"%.2f%@",ingredinete.cantidad,ingredinete.unidad] forState:UIControlStateNormal];
    //le pedimos al servidor que nos de la imagen del ingrdeinte
    [celdaId.imagenIng sd_setImageWithURL:[NSURL URLWithString:[@"https://www.reasype.com" stringByAppendingString:ingredinete.img] ] placeholderImage:[UIImage imageNamed:@"exito"]];
    
    
    return celdaId;
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.listaIngredientes.listaDeIngredientes removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    //manda al detalle del ingrediente
    if ([[segue identifier] isEqualToString:@"detalleLista"]) {
        
        DetalleDespensaTableViewController * detalle = [segue destinationViewController];
        
        detalle.ingredienteAMostrar = [self.listaIngredientes mostrarReceta:[self.tableView indexPathForSelectedRow].row];
        
    }
    
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{//metodo encargado de la busqueda en la searchbar
    
    if(![searchBar isFirstResponder]) {
        // user tapped the 'clear' button
        shouldBeginEditing = NO;
        // do whatever I want to happen when the user clears the search...
    }
    
    if ([searchText length]==0) {//si la cadena no tiene nada se carga la lista principal
        
        [self.resultdoBusqueda removeAllObjects];
        [self.resultdoBusqueda addObjectsFromArray:self.listaIngredientes.listaDeIngredientes];
        
        
    }else{//si tiene algo se cargan todos los elemntos que coincidan con lo ecrito
        
        [self.resultdoBusqueda removeAllObjects];
        for (Ingrediente *i in self.listaIngredientes.listaDeIngredientes) {
            
            NSRange r = [i.nombre rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if ( r.location != NSNotFound) {
                
                [self.resultdoBusqueda addObject:i];
            }
        }
    }
    [self.tableView reloadData];
}


- (IBAction)GuardarIngrediente:(UIStoryboardSegue *)segue
{
    //cuando se llame a este metodo se guardara el ingredietne en la lista de la compra
    
    if ([[segue identifier] isEqualToString:@"guardarIngredientesCompra"]){
        
        AddIngredienteTableViewController* addIngrediente = [segue sourceViewController];
        if (addIngrediente.ingredientesAdd) {
            
            if (addIngrediente.ingredientesAdd) {
                for (Ingrediente * i in addIngrediente.ingredientesAdd) {
                    
                    if ([self.listaIngredientes contieneEsteObjeto:i]) {
                        
                        for (Ingrediente *j in self.listaIngredientes.listaDeIngredientes) {
                            
                            NSRange r = [j.nombre rangeOfString:i.nombre options:NSCaseInsensitiveSearch];
                            
                            if ( r.location != NSNotFound) {
                                
                                j.cantidad = j.cantidad+i.cantidad;
                            }
                        }
                    }else{
                        [self.listaIngredientes addIngredinte:i];
                    }
                }
            }
        }
        [[self tableView] reloadData];
        [self dismissViewControllerAnimated:YES completion:Nil];
        
    }
}

-(void)queIngredientesTenemos{
    
    //se encarga de generar la url para conseguir los ingredientes
    
    NSString * idI = @"";
    
    int logitudaLista = (int)self.listaIngredientes.listaDeIngredientes.count;
    for (int i = 0; i<logitudaLista; i++) {
        
        Ingrediente * ingrediente =[self.listaIngredientes.listaDeIngredientes objectAtIndex:i];
        
        if (i==logitudaLista-1){
            idI = [idI stringByAppendingString:[NSString stringWithFormat:@"%i",ingrediente.idIN]];
        }else {
            idI = [idI stringByAppendingString:[NSString stringWithFormat:@"%i,",ingrediente.idIN]];
        }
    }
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"idIC"];
    
    if (![idI isEqualToString:@""] && savedValue!=nil) {
        if (![savedValue isEqualToString:@""] && idI!=nil) {
        savedValue = [savedValue stringByAppendingString:[NSString stringWithFormat:@",%@",idI]];
        } else {
            savedValue = [savedValue stringByAppendingString:[NSString stringWithFormat:@"%@",idI]];
        }
    } else {
        savedValue = idI;
    }
    
    
    [[NSUserDefaults standardUserDefaults] setObject:savedValue forKey:@"idI"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

//este metodo estar relacionado con el boton y se ecargara de cambiar la cantida de un ingrediente

-(void)abreDialogo:(UIButton *)sender{
    //conseguimos el indexPath
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    //creamos alert con 2 acciones
    UIAlertController *alert= [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"New qty in %@",[[self.listaIngredientes.listaDeIngredientes objectAtIndex:indexPath.row] unidad]] message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        UITextField *textField = alert.textFields[0];
        [(Ingrediente*) [self.listaIngredientes.listaDeIngredientes objectAtIndex:indexPath.row] setCantidad:[textField.text integerValue] ];
        [[self tableView] reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    //añadimos las acciones
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = [NSString stringWithFormat:@"Qty in %@",[[self.listaIngredientes.listaDeIngredientes objectAtIndex:indexPath.row] unidad]];
        textField.keyboardType = UIKeyboardTypeDefault;
    }];
    
    [self presentViewController:alert animated:YES completion:nil];
}


//al presinar el boton de añadir a la despensa se mostrar un aler diciendo si la operacion ha sido correcta

- (IBAction)accionBotonAdd:(id)sender {
    
  
    NSString * idI = @"";
    
    int logitudaLista = (int)self.listaIngredientes.listaDeIngredientes.count;
    for (int i = 0; i<logitudaLista; i++) {
        
        Ingrediente * ingrediente =[self.listaIngredientes.listaDeIngredientes objectAtIndex:i];
        
        if (i==logitudaLista-1){
            idI = [idI stringByAppendingString:[NSString stringWithFormat:@"%i",ingrediente.idIN]];
        }else {
            idI = [idI stringByAppendingString:[NSString stringWithFormat:@"%i,",ingrediente.idIN]];
        }
        
        
        
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:idI forKey:@"idIC"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Correct operation"
                                                                   message:@"The ingredients have been added"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    [self queIngredientesTenemos];
    self.listaIngredientes = [[IngredienteEnCompra alloc] initIngredientes];
    [self.tableView reloadData];
    
}



-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{//indica lo que hacer cuando se clica en el boton de busqueda del teclado
    [searchBar resignFirstResponder];
}
-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{//javi
    self.tableView.allowsSelection = NO;
    [self.view addGestureRecognizer:tap];
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    self.tableView.allowsSelection = YES;
    [self.view removeGestureRecognizer:tap];
    [searchBar resignFirstResponder];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)bar {
    BOOL boolToReturn = shouldBeginEditing;
    shouldBeginEditing = YES;
    return boolToReturn;
}

-(void)dismissKeyboard {
    [self.searchBar resignFirstResponder];
}
//metodo que cierra el teclado cuando se presiona done
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
