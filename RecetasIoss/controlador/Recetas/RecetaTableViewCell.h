//
//  RecetaTableViewCell.h
//  RecetasIoss
//
//  Created by Javi on 5/3/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecetaTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imagen;
@property (weak, nonatomic) IBOutlet UILabel *titulo;
@property (weak, nonatomic) IBOutlet UILabel *ingredientes;

@end
