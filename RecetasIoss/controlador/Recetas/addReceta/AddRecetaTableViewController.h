//
//  AddRecetaTableViewController.h
//  RecetasIoss
//
//  Created by pablo Gutierrez on 6/2/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Receta.h"

@interface AddRecetaTableViewController : UITableViewController <UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIGestureRecognizerDelegate>

//receta a guardar 

@property Receta * nuevaReceta;
@property BOOL estaBien;

@end
