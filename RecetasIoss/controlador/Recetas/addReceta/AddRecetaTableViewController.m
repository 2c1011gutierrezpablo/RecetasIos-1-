//
//  AddRecetaTableViewController.m
//  RecetasIoss
//
//  Created by pablo Gutierrez on 6/2/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import "AddRecetaTableViewController.h"
#import "AddIngredienteTableViewController.h"
#import "Ingrediente.h"
#import "Ingredientes.h"
#import "Post.h"

@interface AddRecetaTableViewController ()


@property (weak, nonatomic) IBOutlet UITextView *descripcion;
@property (weak, nonatomic) IBOutlet UITextField *titulo;
@property (weak, nonatomic) IBOutlet UIImageView *Imagen;
- (IBAction)introduceNumeroDePasos:(id)sender;
@property NSArray * listaNumeroDePasos;

@property (weak, nonatomic) IBOutlet UIButton *botonPasos;

@property NSMutableArray * listaDeIngredientes;
@property (weak, nonatomic) IBOutlet UITextView *c1;
@property (weak, nonatomic) IBOutlet UITextView *c2;
@property (weak, nonatomic) IBOutlet UITextView *c3;
@property (weak, nonatomic) IBOutlet UITextView *c4;
@property (weak, nonatomic) IBOutlet UITextView *c5;
@property (weak, nonatomic) IBOutlet UITextView *c6;
@property (weak, nonatomic) IBOutlet UITextView *c7;
@property (weak, nonatomic) IBOutlet UITextView *c8;
@property (weak, nonatomic) IBOutlet UITextView *c9;
@property (weak, nonatomic) IBOutlet UITextView *c10;

@end

@implementation AddRecetaTableViewController{
    
    int numeroPasos;
    
}

- (void)viewDidLoad {
    
    //lista con numero de pasos disponible 
    self.listaNumeroDePasos = @[ @"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10"];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(profilePicAction:)];
    singleTap.numberOfTapsRequired = 1;
    singleTap.numberOfTouchesRequired = 1;
    singleTap.delegate = self;
    
    [self.Imagen addGestureRecognizer:singleTap];
    
    [super viewDidLoad];
    

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    //si se necesita algun paso se usran dos secciones sino solo una
    
    if (numeroPasos>0) {
        return 2;
    }else{
        return 1;
    }
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    //si se necesitan pasos se usaran tantos como se selecione sino solo se mostran los campos necesarios
    
    if (numeroPasos>0) {
       
        if (section==0) {
            return 5;
        } else {
            return numeroPasos;
        }
        
    } else {
        return 5;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //se encarga de cerar la table con celdas estaticas
    
    UITableViewCell *celda  = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    
    return celda;
    
    }




- (IBAction)introduceNumeroDePasos:(id)sender {
    
    
    //al presionar el boton numero de pasos se abriara un alert con tantos pasos como se necesit
   int i = 0;
    //se crea el alert y se añaden tantos campos de accion como numeros de pasos hay, cundo se seleccione uno de ellos se recargala la tabla con los pasos selccionados
    UIAlertController *alert  = [UIAlertController alertControllerWithTitle:@"Introduce" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (NSString *item in self.listaNumeroDePasos) {
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:item style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [self addSeleccion:i];
        }];
        
        [alert addAction:defaultAction];
        i++;
    
    }
    
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)addSeleccion:(NSInteger)row {
    
    //este metodo lo tiene sigandos los botones del aler superior y se encargan de añadir tantos el numero de pasos selecionado
    
   numeroPasos = [[self.listaNumeroDePasos objectAtIndex:row] intValue];
    [self.tableView reloadData];
}



-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    //if ([self todoBienIntroducido]) {
        if ([[segue identifier] isEqualToString:@"addReceta"]) {
            self.nuevaReceta = [[Receta alloc] initReceta:self.titulo.text ydescripcion:self.descripcion.text ypasos:[self  dameNumeroDePasos] yingredientes:self.listaDeIngredientes yUrlImagen:@""];
            //[Post sendPostReceta:self.nuevaReceta];
            self.estaBien = YES;
        }
        
    //}else{
        
       // self.estaBien = NO;
        
    //}
    
}


-(NSMutableArray*)dameNumeroDePasos{
    NSMutableArray * listaPasos = [[NSMutableArray alloc] init];
    
    [listaPasos addObject:self.c1.text];
    [listaPasos addObject:self.c2.text];
    [listaPasos addObject:self.c3.text];
    [listaPasos addObject:self.c4.text];
    [listaPasos addObject:self.c5.text];
    [listaPasos addObject:self.c6.text];
    [listaPasos addObject:self.c7.text];
    [listaPasos addObject:self.c8.text];
    [listaPasos addObject:self.c9.text];
    [listaPasos addObject:self.c10.text];
    
    NSMutableArray  * pasoAenviar= [[NSMutableArray alloc] init];
    
    for (int i = 0; i<numeroPasos; i++) {
        [pasoAenviar addObject: [listaPasos objectAtIndex:i]];
    }
    
    return pasoAenviar;
}

-(bool)todoBienIntroducido{
    
    if (![self.titulo.text isEqualToString:@""]) {
        if (![self.descripcion.text isEqualToString:@""]) {
            
            
            NSArray * a = [self dameNumeroDePasos];
            
            for (UITextView * i in a) {
                if (![i.text isEqualToString:@""]) {
                    return YES;
                }
            }
            
            
        }
    }
    return NO;
}

-(NSMutableArray*)addIngrediente:(NSArray*)pasos{
    //con los pasos introducidos se genra esta lista
    
    return nil;
}


- (IBAction)GuardarIngrediente:(UIStoryboardSegue *)segue
{
    //cuando se llame a este metodo se guardara el ingredietne en la lista de la compra
    
    if ([[segue identifier] isEqualToString:@"guardarIngredientesCompra"]){
        
        
        AddIngredienteTableViewController* addReceta = [segue sourceViewController];
        
        if (addReceta.ingredientesAdd) {
            for (Ingrediente * i in addReceta.ingredientesAdd) {
                [self.listaDeIngredientes addObject:i];
            }
            
        }
        [self dismissViewControllerAnimated:YES completion:Nil];
        
        
        
    }
}

- (IBAction)profilePicAction:(id)sender {
    
    UIAlertController *alertController=[UIAlertController alertControllerWithTitle:@"" message:@"Change Profile image" preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *takePhoto=[UIAlertAction actionWithTitle:@"Take Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        
        picker.delegate = self;
        
        picker.allowsEditing = YES;
        
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:NULL];
        
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    [alertController addAction:takePhoto];
    
    UIAlertAction *choosePhoto=[UIAlertAction actionWithTitle:@"Select From Photos" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        UIImagePickerController *pickerView = [[UIImagePickerController alloc] init];
        
        pickerView.allowsEditing = YES;
        
        pickerView.delegate = self;
        
        [pickerView setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        
        [self presentViewController:pickerView animated:YES completion:nil];
        
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertController addAction:choosePhoto];
    
    UIAlertAction *actionCancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
        
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertController addAction:actionCancel];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    
    self.Imagen.image = chosenImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}


@end
