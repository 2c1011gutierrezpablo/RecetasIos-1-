//
//  DetalleRecetasTableViewController.m
//  RecetasIoss
//
//  Created by pablo Gutierrez on 1/2/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import "DetalleRecetasTableViewController.h"
#import "PasosViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Ingrediente.h"

@interface DetalleRecetasTableViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *Imagen;
@property (weak, nonatomic) IBOutlet UITextView *textViewDescripcion;
@property (weak, nonatomic) IBOutlet UILabel *ingredientes;
@property (weak, nonatomic) IBOutlet UILabel *numPasos;

@end

@implementation DetalleRecetasTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = self.laReceta.titulo;//nombre de la receta represnetado en la barra
   [self.Imagen sd_setImageWithURL:[NSURL URLWithString:[@"https://www.reasype.com" stringByAppendingString:self.laReceta.urlImagen] ] placeholderImage:nil];
        self.ingredientes.text = [self.laReceta getIngredientesString] ;
    self.textViewDescripcion.text = self.laReceta.descripcion ;
    self.numPasos.text = [NSString stringWithFormat:@"%lu steps",(unsigned long)[self.laReceta.ingredientesDeLaReceta count] ];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//se encarga de cargar los pasos para poder hacer la receta
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"vewPasos"]) {
        
        [self ingredientesReceta];
        
        PasosViewController * vewPasos = [segue destinationViewController];
        
        vewPasos.pasos = self.laReceta.pasos;
        
        
    }
}
-(void)ingredientesReceta{
    
    //se encarga de generar la url para conseguir los ingredientes
    
    NSString * idI = @"";
    
    int logitudaLista = (int)self.laReceta.ingredientesDeLaReceta.count;
    for (int i = 0; i<logitudaLista; i++) {
        
        Ingrediente * ingrediente =[self.laReceta.ingredientesDeLaReceta objectAtIndex:i];
        
        if (i==logitudaLista-1){
            idI = [idI stringByAppendingString:[NSString stringWithFormat:@"%i",ingrediente.idIN]];
        }else {
            idI = [idI stringByAppendingString:[NSString stringWithFormat:@"%i,",ingrediente.idIN]];
        }
        
        
        
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:idI forKey:@"idIAborrar"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}


@end
