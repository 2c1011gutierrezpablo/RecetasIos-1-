//
//  DetalleRecetasTableViewController.h
//  RecetasIoss
//
//  Created by pablo Gutierrez on 1/2/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Receta.h"

@interface DetalleRecetasTableViewController : UITableViewController

@property Receta * laReceta;//es la receta a mstrar en el detalle

@end
