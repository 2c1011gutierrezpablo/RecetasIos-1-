//
//  PasosViewController.m
//  RecetasIoss
//
//  Created by pablo Gutierrez on 2/2/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import "PasosViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Pasos.h"


@interface PasosViewController ()


@property (weak, nonatomic) IBOutlet UITextView *textViewDescripcion;//texto del paso
- (IBAction)accionBotonDeSiguiente:(id)sender;
- (IBAction)botonFin:(id)sender;
- (IBAction)pasoAnterior:(id)sender;
- (IBAction)accionStepper:(UIStepper*)sender;
- (IBAction)botonPlay:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *botonPlay;//inicio audio
@property (weak, nonatomic) IBOutlet UIStepper *TamanoStepper;//controlador del tamaño
@property (weak, nonatomic) IBOutlet UIImageView *imagenPaso;

@property int numeroDePasos;

@end

@implementation PasosViewController{
    
    int tamanoFuente;
    BOOL encendido;
}

BOOL speechPaused = 0;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    encendido= true;
    
    self.numeroDePasos = 0;//inicia el contador de control de numero de pasos
    tamanoFuente = 14.0;
    [self pintaPasos];
    
    self.synthesizer = [[AVSpeechSynthesizer alloc] init];//iniciamos el sitetizador para habalr 
    speechPaused = NO;
    self.synthesizer.delegate = self;
    
    self.TamanoStepper.minimumValue = tamanoFuente;//establecemos el tamaño minimo de la letra
    self.TamanoStepper.maximumValue = tamanoFuente+20;// establecemos el tamaño maximo de la letra
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)pintaPasos{//se encarga de cambiar de paso cuando se le llama
    
    if (self.numeroDePasos<(int) self.pasos.count) {
        
        self.navigationItem.title = [NSString stringWithFormat:@"Step %i",self.numeroDePasos+1];
        Pasos* paso = [self.pasos objectAtIndex:self.numeroDePasos];
        self.textViewDescripcion.text = paso.descripcion;
        [self.imagenPaso sd_setImageWithURL:[NSURL URLWithString:[@"https://www.reasype.com" stringByAppendingString:paso.urlImagen] ] placeholderImage:nil];

        
    }
    
    
}

- (IBAction)accionBotonDeSiguiente:(id)sender {//se encarga de gestionar las pulsaciones del boton
    
    if (self.numeroDePasos<(int) self.pasos.count ) {
       
        self.numeroDePasos = self.numeroDePasos+1;
        [self pintaPasos];
        
    }
    
}

- (IBAction)botonFin:(id)sender {//nos manda al principo poe que hemos terminado
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"the ingredients have been erased from the pantry"
                                                                   message:@""
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                               [self performSegueWithIdentifier:@"vueltaADespensa" sender:self];
                                                              
                                                          }];
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    
   
}

- (IBAction)pasoAnterior:(id)sender {//indica el paso anterior de la receta
    
    if ( self.numeroDePasos>0 ) {
        self.numeroDePasos = self.numeroDePasos-1;
        [self pintaPasos];
        
        if (self.numeroDePasos>(int) self.pasos.count) {
            //[self.boton setTitle:@"siguente" forState:UIControlStateNormal];
        }
    }
    
}

- (IBAction)accionStepper:(UIStepper*)sender {//metodo para el aumento o disminucion del tamaño de la letra
     double value = [sender value];
    [self.textViewDescripcion setFont:[UIFont fontWithName:@"arial"  size:value]];
    
}



- (IBAction)botonPlay:(id)sender {//metodo encargado de leer el texto
    
    [self.textViewDescripcion resignFirstResponder];
    
    
    if (speechPaused == NO) {
        
        [self.synthesizer continueSpeaking];
        speechPaused = YES;
        
    } else {
        
        speechPaused = NO;
        [self.synthesizer pauseSpeakingAtBoundary:AVSpeechBoundaryImmediate];
        
    }
    if (self.synthesizer.speaking == NO) {
        //iniciamos el lector de texto
        AVSpeechUtterance *utterance = [[AVSpeechUtterance alloc] initWithString:self.textViewDescripcion.text];
        //idoma en el que leera el texto
        utterance.voice = [AVSpeechSynthesisVoice voiceWithLanguage:@"es-ES"];
        [self.synthesizer speakUtterance:utterance];
        
    }
    
}
- (IBAction)escuchaComando:(id)sender {//metodo encargado de iniciar la escucha de microfono
    
    //reconocedor_de_voz * rec = [[reconocedor_de_voz alloc] init];
    encendido = !encendido;
    [self iniciaReconocedro:encendido];
    [self  microPhoneTapped];
    
    
    
}
//se encarga de iniciar el reconocerdor de voz en el idioma indicado
-(void)iniciaReconocedro:(BOOL)estado{
    
    speechRecognizer = [[SFSpeechRecognizer alloc] initWithLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    
    speechRecognizer.delegate = self;
    
}

- (void)startListening {
    
   // comrpueba que esta disponible el audio
    audioEngine = [[AVAudioEngine alloc] init];
    
    // Make sure there's not a recognition task already running
    if (recognitionTask) {
        [recognitionTask cancel];
        recognitionTask = nil;
    }
    
    // inicia el reconocedor
    NSError *error;
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryRecord error:&error];
    [audioSession setActive:YES withOptions:AVAudioSessionSetActiveOptionNotifyOthersOnDeactivation error:&error];
    
    
    
    recognitionRequest = [[SFSpeechAudioBufferRecognitionRequest alloc] init];
    AVAudioInputNode *inputNode = audioEngine.inputNode;
    recognitionRequest.shouldReportPartialResults = YES;
    
    recognitionTask = [speechRecognizer recognitionTaskWithRequest:recognitionRequest resultHandler:^(SFSpeechRecognitionResult * _Nullable result, NSError * _Nullable error) {
        BOOL isFinal = NO;
        if (result) {
            
            //este es el araia donde se registra el audio y donde lanzamos los comandos a realizar
            
            NSArray * palabrasDichas = [result.bestTranscription.formattedString componentsSeparatedByString:@" "];
            NSString * ultimaPalabraDicha = [palabrasDichas objectAtIndex:palabrasDichas.count-1];
            NSLog(@"RESULT:%@",ultimaPalabraDicha);
            
            if ( [ultimaPalabraDicha caseInsensitiveCompare:@"next"]== NSOrderedSame) {
                
                
                [self accionBotonDeSiguiente:nil];
                NSLog(@"hemos pasado a la siguiente receta");
                
            }else if ([ultimaPalabraDicha caseInsensitiveCompare:@"finally"]== NSOrderedSame) {
                
                NSLog(@"cerramo receta");
                
                [self botonFin:nil];
                
            }else if ([ultimaPalabraDicha caseInsensitiveCompare:@"previous"]== NSOrderedSame) {
                
                NSLog(@"vamos a la receta anterior");
                
                [self pasoAnterior:nil];
            }else if ([ultimaPalabraDicha caseInsensitiveCompare:@"read"]== NSOrderedSame) {
                
                
                
                [self botonPlay:nil];
            }
            
            isFinal = !result.isFinal;
        }
        if (error) {
            [audioEngine stop];
            [inputNode removeTapOnBus:0];
            recognitionRequest = nil;
            recognitionTask = nil;
        }
    }];
    
    //establece el formato de gabracion
    AVAudioFormat *recordingFormat = [inputNode outputFormatForBus:0];
    [inputNode installTapOnBus:0 bufferSize:1024 format:recordingFormat block:^(AVAudioPCMBuffer * _Nonnull buffer, AVAudioTime * _Nonnull when) {
        [recognitionRequest appendAudioPCMBuffer:buffer];
    }];
    
    // comienza a escuchar
    [audioEngine prepare];
    [audioEngine startAndReturnError:&error];
    NSLog(@"Che estoy escuchando");
}


//se encarga de despertar al microfono
- (void)microPhoneTapped {
    if (audioEngine.isRunning) {
        [audioEngine stop];
        [recognitionRequest endAudio];
    } else {
        [self startListening];
    }
}

#pragma mark - SFSpeechRecognizerDelegate Delegate Methods
//se encarga de decir si el reconocedor de voz esta disponible
- (void)speechRecognizer:(SFSpeechRecognizer *)speechRecognizer availabilityDidChange:(BOOL)available {
    NSLog(@"Availability:%d",available);
}



@end
