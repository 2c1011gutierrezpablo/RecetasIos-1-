//
//  PasosViewController.h
//  RecetasIoss
//
//  Created by pablo Gutierrez on 2/2/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <Speech/Speech.h>

@interface PasosViewController : UIViewController <AVSpeechSynthesizerDelegate, SFSpeechRecognizerDelegate> {
    SFSpeechRecognizer *speechRecognizer;
    SFSpeechAudioBufferRecognitionRequest *recognitionRequest;
    SFSpeechRecognitionTask *recognitionTask;
    AVAudioEngine *audioEngine;
}

-(void)iniciaReconocedro:(BOOL)estado;
- (void)microPhoneTapped;
- (void)speechRecognizer:(SFSpeechRecognizer *)speechRecognizer availabilityDidChange:(BOOL)available;

@property NSArray * pasos;//lista con los pasos
@property (strong, nonatomic) AVSpeechSynthesizer *synthesizer;


@end
