//
//  RecetasTableViewController.m
//  RecetasIoss
//
//  Created by pablo Gutierrez on 1/2/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import "RecetasTableViewController.h"
#import "Receta.h"
#import "ListaDeRecetas.h"
#import "Ingrediente.h"
#import "DetalleRecetasTableViewController.h"
#import "AddRecetaTableViewController.h"
#import "botonEspecial.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "RecetaTableViewCell.h"
#import "Post.h"

@interface RecetasTableViewController (){
    BOOL shouldBeginEditing;
    UITapGestureRecognizer *tap;
}

@property ListaDeRecetas * laListaDeRecetas;//lisa de rcetas que se dispone
@property(strong,nonatomic) NSMutableArray *resultdoBusqueda;//almacena la busqueda
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property UIImageView *fondo;

@end

@implementation RecetasTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.fondo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fondoRecetas"]];
    self.fondo.contentMode = UIViewContentModeScaleAspectFill;
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];//creamos un tab para poder cerrar el teclado pulsando fuera del
    
    //hacemos la carga de receta asicronamente 
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        //iniciamos las dos listas
        
        self.laListaDeRecetas = [[ListaDeRecetas alloc] initRecetas];
        self.resultdoBusqueda = [[NSMutableArray alloc] init];
        
        shouldBeginEditing = YES;
        
        
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            
            [self.tableView reloadData];
        
        });
    });
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    if (self.laListaDeRecetas.ListaDeRecetas.count>0) {
        self.navigationItem.largeTitleDisplayMode = UINavigationItemLargeTitleDisplayModeAutomatic;
        self.navigationItem.leftBarButtonItem.enabled = YES;
        self.searchBar.hidden = NO;
        [self.tableView willRemoveSubview:self.fondo];
        self.tableView.backgroundView = nil;
        [self.tableView setSeparatorColor:[UIColor lightGrayColor]];
        
    } else {
        self.navigationItem.largeTitleDisplayMode = UINavigationItemLargeTitleDisplayModeNever;
        self.navigationItem.leftBarButtonItem.enabled = NO;
        self.searchBar.hidden = YES;
        [self.tableView addSubview:self.fondo];
        [self.tableView setSeparatorColor:[UIColor clearColor]];
        self.tableView.backgroundView = self.fondo;
        self.tableView.backgroundView.contentMode = UIViewContentModeCenter;
        [self setEditing:NO];
        [self.tableView setEditing:NO animated:YES];
    }
    if ([self.resultdoBusqueda count]>0) {// si no tiene nada la lista de busqueda se crean tantas celdas como la lista principal tenga
        return [self.resultdoBusqueda count];
    } else {
        return self.laListaDeRecetas.ListaDeRecetas.count;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    RecetaTableViewCell *celda = [tableView dequeueReusableCellWithIdentifier:@"celdaReceta" forIndexPath:indexPath];
    
    if (celda == nil) {
        celda = [[RecetaTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"celdaReceta"];
    }
    
    Receta * r1 = nil;
    
    
    if ([self.resultdoBusqueda count]>0) {//si la cadena de busqueda no tiene nada se carga la lista de serie sino la de busqueda
        
        r1 = [self.resultdoBusqueda objectAtIndex:indexPath.row];
       
    }else{
        
       r1 = [self.laListaDeRecetas mostrarReceta:indexPath.row];
        
    }
    [[celda titulo]setText:r1.titulo];
    [[celda ingredientes]setText:[r1 getIngredientesString]];
    [celda.imagen sd_setImageWithURL:[NSURL URLWithString:[@"https://www.reasype.com" stringByAppendingString:r1.urlImagen] ] placeholderImage:nil];
    
    return celda;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row<[self.laListaDeRecetas.ListaDeRecetas count]) {
        return 200;
    }
    return 0;
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{//metodo encargado de la busqueda en la searchbar
    
    if(![searchBar isFirstResponder]) {
        // user tapped the 'clear' button
        shouldBeginEditing = NO;
        // do whatever I want to happen when the user clears the search...
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // No explicit autorelease pool needed here.
        // The code runs in background, not strangling
        // the main run loop.
        if ([searchText length]==0) {//si la cadena no tiene nada se carga la lista principal
            
            [self.resultdoBusqueda removeAllObjects];
            [self.resultdoBusqueda addObjectsFromArray:self.laListaDeRecetas.ListaDeRecetas];
        }else{//si tiene algo se cargan todos los elemntos que coincidan con lo ecrito
            
            [self.resultdoBusqueda removeAllObjects];
            for (Receta *i in self.laListaDeRecetas.ListaDeRecetas) {
                
                NSRange r = [i.titulo rangeOfString:searchText options:NSCaseInsensitiveSearch];
                
                if ( r.location != NSNotFound) {
                    
                    [self.resultdoBusqueda addObject:i];
                }
            }
        }
        dispatch_sync(dispatch_get_main_queue(), ^{
            // This will be called on the main thread, so that
            // you can update the UI, for example.
            [self.tableView reloadData];
        });
    });
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{//indica lo que hacer cuando se clica en el boton de busqueda del teclado
    [searchBar resignFirstResponder];
}
-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    self.tableView.allowsSelection = NO;
    [self.view addGestureRecognizer:tap];
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    self.tableView.allowsSelection = YES;
    [self.view removeGestureRecognizer:tap];
    [searchBar resignFirstResponder];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)bar {
    BOOL boolToReturn = shouldBeginEditing;
    shouldBeginEditing = YES;
    return boolToReturn;
}

-(void)dismissKeyboard {
    [self.searchBar resignFirstResponder];
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"detalleReceta"]) {
        
        //cargamos el detalle de cada receta
        DetalleRecetasTableViewController * detalle = [segue destinationViewController];
        
        detalle.laReceta = [self.laListaDeRecetas mostrarReceta:[self.tableView indexPathForSelectedRow].row];
        
    }
}

- (IBAction)cancel:(UIStoryboardSegue *)segue
{
    
    if ([[segue identifier] isEqualToString:@"CancelarSegue"]) {
        [self dismissViewControllerAnimated:YES completion:NULL];
    }
}

-(IBAction)guardarReceta:(UIStoryboardSegue *)segue {
    if ([[segue identifier] isEqualToString:@"addReceta"]){
        
        //si se crea una nueva receta se usara añadira aqui
        
        AddRecetaTableViewController* addReceta = [segue sourceViewController];
        
        if (addReceta.nuevaReceta) {
            [self.laListaDeRecetas addReceta:addReceta.nuevaReceta];
            [[self tableView] reloadData];
            [Post sendPostReceta:addReceta.nuevaReceta];
        }
        [self dismissViewControllerAnimated:YES completion:Nil];
        
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)viewDidAppear:(BOOL)animated{
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"idI"];
    
    if (![savedValue isEqualToString:@""] && savedValue !=nil) {
        
        self.laListaDeRecetas = [[ListaDeRecetas alloc] initRecetas];
        [self.tableView reloadData];
        NSLog(@"voy a recargarme");
        
    }else{
        self.laListaDeRecetas = [[ListaDeRecetas alloc] initRecetas];
        [self.tableView reloadData];
    }
    
}



@end
