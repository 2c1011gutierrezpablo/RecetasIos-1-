//
//  RecetasTableViewController.h
//  RecetasIoss
//
//  Created by pablo Gutierrez on 1/2/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecetasTableViewController : UITableViewController <UITableViewDataSource,UISearchBarDelegate, UITableViewDelegate>

@end
