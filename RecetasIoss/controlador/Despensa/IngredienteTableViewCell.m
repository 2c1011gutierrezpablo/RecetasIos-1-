//
//  IngredienteTableViewCell.m
//  RecetasIoss
//
//  Created by Javi on 5/3/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import "IngredienteTableViewCell.h"

@implementation IngredienteTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    self.imagenIng.layer.cornerRadius = 25;
    self.imagenIng.layer.masksToBounds = YES;
}

@end
