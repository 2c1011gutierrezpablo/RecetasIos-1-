//
//  DetalleDespensaTableViewController.h
//  RecetasIoss
//
//  Created by pablo Gutierrez on 30/1/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Ingrediente.h"

@interface DetalleDespensaTableViewController : UITableViewController



@property Ingrediente * ingredienteAMostrar;//es el ingrediente que se mostrara en eta viewe

@end
