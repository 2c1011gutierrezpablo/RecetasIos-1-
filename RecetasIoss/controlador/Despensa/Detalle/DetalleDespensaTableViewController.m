//
//  DetalleDespensaTableViewController.m
//  RecetasIoss
//
//  Created by pablo Gutierrez on 30/1/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import "DetalleDespensaTableViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface DetalleDespensaTableViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imagenIngrediente;

@end

@implementation DetalleDespensaTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.navigationItem.title = self.ingredienteAMostrar.nombre;
    //se encarga de poner el nombre del ingredinte en el titulo de la view
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //seran celdas estaticas
    return 3;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *celda  = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/YYYY"];
    
    switch (indexPath.row) {
        case 0:
            //carga la imagen del servidor en cache
            [self.imagenIngrediente sd_setImageWithURL:[NSURL URLWithString:[@"https://www.reasype.com" stringByAppendingString:self.ingredienteAMostrar.img] ] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
            
            break;
        case 1:
            //asigna la cantidad del ingrediente
            [[celda detailTextLabel]setText: [NSString stringWithFormat:@"%.2f%@",self.ingredienteAMostrar.cantidad,self.ingredienteAMostrar.unidad] ];
            break;
        case 2:
            
            //asinga la descripcion
            
            [[celda detailTextLabel]setText: self.ingredienteAMostrar.nombre];
            break;
        
    }
    
    return celda;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
