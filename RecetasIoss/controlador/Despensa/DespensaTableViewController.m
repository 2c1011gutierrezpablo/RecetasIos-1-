//
//  DespensaTableViewController.m
//  RecetasIoss
//
//  Created by pablo Gutierrez on 28/1/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import "DespensaTableViewController.h"
#import "Ingredientes.h"
#import "Ingrediente.h"
#import "DetalleDespensaTableViewController.h"
#import "AddIngredienteTableViewController.h"
#import "botonEspecial.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "IngredienteTableViewCell.h"


@interface DespensaTableViewController () {
    BOOL shouldBeginEditing;
    UITapGestureRecognizer *tap;
}

@property Ingredientes * listaIngredientes;//es la clase que contiene todso los ingredientes
@property(strong,nonatomic) NSMutableArray *resultdoBusqueda;//almacena la busqueda
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property UIImageView *fondo;


@property (weak, nonatomic) IBOutlet UITextView *c1;
@property (weak, nonatomic) IBOutlet UITextView *c2;
@property (weak, nonatomic) IBOutlet UITextView *c3;
@property (weak, nonatomic) IBOutlet UITextView *c4;
@property (weak, nonatomic) IBOutlet UITextView *c5;
@property (weak, nonatomic) IBOutlet UITextView *c6;
@property (weak, nonatomic) IBOutlet UITextView *c7;
@property (weak, nonatomic) IBOutlet UITextView *c8;
@property (weak, nonatomic) IBOutlet UITextView *c9;
@property (weak, nonatomic) IBOutlet UITextView *c10;


@end

@implementation DespensaTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
    self.fondo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fondoDespensa"]];
    self.fondo.contentMode = UIViewContentModeScaleAspectFill;
    

    //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{//todo lo que se realice se hara de forma asincrona
        
        self.listaIngredientes = [[Ingredientes alloc] initIngredientes:0];//inicia la lista de ingredientes
        
        self.resultdoBusqueda = [[NSMutableArray alloc] init];//inicia la lista de ingredientes que se mostraran cuando se busque
        
        shouldBeginEditing = YES;
        
        tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];//creamos un tab para poder cerrar el teclado pulsando fuera del
        
        //dispatch_sync(dispatch_get_main_queue(), ^{
           
           // [self.tableView reloadData];//cuando termine todo lo que se realiza en el block se recargara la tabla
       // });
   // });
    
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;//solo  habra una seccion
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if ([self.listaIngredientes longitudLista]>0) {
        self.navigationItem.largeTitleDisplayMode = UINavigationItemLargeTitleDisplayModeAutomatic;
        self.navigationItem.leftBarButtonItem.enabled = YES;
        self.searchBar.hidden = NO;
        [self.tableView willRemoveSubview:self.fondo];
        self.tableView.backgroundView = nil;
        [self.tableView setSeparatorColor:[UIColor lightGrayColor]];

    } else {
        self.navigationItem.largeTitleDisplayMode = UINavigationItemLargeTitleDisplayModeNever;
        self.navigationItem.leftBarButtonItem.enabled = NO;
        self.searchBar.hidden = YES;
        [self.tableView addSubview:self.fondo];
        [self.tableView setSeparatorColor:[UIColor clearColor]];
        self.tableView.backgroundView = self.fondo;
        self.tableView.backgroundView.contentMode = UIViewContentModeCenter;
        [self setEditing:NO];
        [self.tableView setEditing:NO animated:YES];
    }
   if ([self.resultdoBusqueda count]>0) {// si no tiene nada la lista de busqueda se crean tantas celdas como la lista principal tenga
       return [self.resultdoBusqueda count];
       
   } else {
        return [self.listaIngredientes longitudLista];
   }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {//se encarga de cargar cada celda
    IngredienteTableViewCell *celdaId = [tableView dequeueReusableCellWithIdentifier:@"celdaIngrediente" forIndexPath:indexPath];
    if (celdaId == nil) {//si la celda esta a null se inicia de serie
        celdaId = [[IngredienteTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"celdaIngrediente"];
    }
    
    botonEspecial *buttonCantidad = (botonEspecial *)[celdaId viewWithTag:101];//creamos un boton especial el cual tendra una propiedad
    buttonCantidad.posicion = (int)indexPath.row;//le asignamos la posicion de la celda
    [buttonCantidad addTarget:self action:@selector(abreDialogo:) forControlEvents:UIControlEventTouchUpInside];//le añadimos al boton el metodo a ejecutar cuando se pulse
    Ingrediente * ingredinete = nil;//iniciamos el ingrediente a mostrar
    
    if ([self.resultdoBusqueda count]>0) {//si la cadena de busqueda no tiene nada se carga la lista de serie sino la de busqueda
        ingredinete = [self.resultdoBusqueda objectAtIndex:indexPath.row];//sacamos el ingrediente de la busqueda
    }else{
        ingredinete = [self.listaIngredientes mostrarReceta:indexPath.row];//sacamos el ingrdiente de la lista global
    }
    
    [[celdaId tituloIng]setText:ingredinete.nombre];//asignamos a la celda el nombre del ingrdiente
    //ponemos el valor de la cantidad al boton especial
    [buttonCantidad  setTitle:[NSString stringWithFormat:@"%.2f%@",ingredinete.cantidad,ingredinete.unidad] forState:UIControlStateNormal];
    //le pedimos al servidor que nos de la imagen del ingrdeinte
    [celdaId.imagenIng sd_setImageWithURL:[NSURL URLWithString:[@"https://www.reasype.com" stringByAppendingString:ingredinete.img] ] placeholderImage:[UIImage imageNamed:@"exito"]];

   return celdaId;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {//manda al detalle del ingrediente
    if ([[segue identifier] isEqualToString:@"detalleDespensa"]) {
        DetalleDespensaTableViewController * detalle = [segue destinationViewController];
        //asignamos al detalle el ingrediente seleccionado
        detalle.ingredienteAMostrar = [self.listaIngredientes mostrarReceta:[self.tableView indexPathForSelectedRow].row];
    } else if ([[segue identifier] isEqualToString:@"addDespensa"]) {
        AddIngredienteTableViewController * add = [segue destinationViewController];
        add.listaIngrediente = self.listaIngredientes.ListaIngredientes;//aqui abra que poner la lista del servidro
    }
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.listaIngredientes.ListaIngredientes removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [self borraIngredientesDespensa];
        [self queIngredientesTenemos];
    }
}

- (IBAction)GuardarIngrediente:(UIStoryboardSegue *)segue {
    if ([[segue identifier] isEqualToString:@"guardarIngredientesCompra"]){//al presionar el boton de save se guardara una lista con los ingrdeitnes seleccionados
        AddIngredienteTableViewController* addReceta = [segue sourceViewController];
        
        if (addReceta.ingredientesAdd) {
            for (Ingrediente * i in addReceta.ingredientesAdd) {
                
                if ([self.listaIngredientes contieneEsteObjeto:i]) {
                    
                    for (Ingrediente *j in self.listaIngredientes.ListaIngredientes) {
                        
                        NSRange r = [j.nombre rangeOfString:i.nombre options:NSCaseInsensitiveSearch];
                        
                        if ( r.location != NSNotFound) {
                            
                            j.cantidad = j.cantidad+i.cantidad;
                        }
                    }
                }else{
                    [self.listaIngredientes addIngredinte:i];
                }
            }
        }
        [self.tableView reloadData];
        [self dismissViewControllerAnimated:YES completion:Nil];
        [self queIngredientesTenemos];
    }
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{//metodo encargado de la busqueda en la searchbar

    if(![searchBar isFirstResponder]) {
        // user tapped the 'clear' button
        shouldBeginEditing = NO;
        // do whatever I want to happen when the user clears the search...
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // No explicit autorelease pool needed here.
        // The code runs in background, not strangling
        // the main run loop.
        if ([searchText length]==0) {//si la cadena no tiene nada se carga la lista principal
            
            [self.resultdoBusqueda removeAllObjects];
            [self.resultdoBusqueda addObjectsFromArray:self.listaIngredientes.ListaIngredientes];
        }else{//si tiene algo se cargan todos los elemntos que coincidan con lo ecrito
            
            [self.resultdoBusqueda removeAllObjects];
            for (Ingrediente *i in self.listaIngredientes.ListaIngredientes) {
                
                NSRange r = [i.nombre rangeOfString:searchText options:NSCaseInsensitiveSearch];
                
                if ( r.location != NSNotFound) {
                    
                    [self.resultdoBusqueda addObject:i];
                }
            }
        }
        dispatch_sync(dispatch_get_main_queue(), ^{
            // This will be called on the main thread, so that
            // you can update the UI, for example.
            [self.tableView reloadData];
        });
    });
   
    
    
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{//indica lo que hacer cuando se clica en el boton de busqueda del teclado
    [searchBar resignFirstResponder];
}
-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    self.tableView.allowsSelection = NO;
    [self.view addGestureRecognizer:tap];
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    self.tableView.allowsSelection = YES;
    [self.view removeGestureRecognizer:tap];
    [searchBar resignFirstResponder];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)bar {
    BOOL boolToReturn = shouldBeginEditing;
    shouldBeginEditing = YES;
    return boolToReturn;
}

-(void)dismissKeyboard {
    [self.searchBar resignFirstResponder];
}

//este metodo estar relacionado con el boton y se ecargara de cambiar la cantida de un ingrediente

-(void)abreDialogo:(UIButton *)sender{
    //conseguimos el indexPath
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    //creamos alert con 2 acciones
    UIAlertController *alert= [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"New qty in %@",[[self.listaIngredientes.ListaIngredientes objectAtIndex:indexPath.row] unidad]] message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        UITextField *textField = alert.textFields[0];
        [(Ingrediente*) [self.listaIngredientes.ListaIngredientes objectAtIndex:indexPath.row] setCantidad:[textField.text integerValue] ];
        [[self tableView] reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    //añadimos las acciones
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = [NSString stringWithFormat:@"Qty in %@",[[self.listaIngredientes.ListaIngredientes objectAtIndex:indexPath.row] unidad]];
        textField.keyboardType = UIKeyboardTypeDefault;
    }];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)queIngredientesTenemos{
    
    //se encarga de generar la url para conseguir los ingredientes 
    
    NSString * idI = @"";
    
    int logitudaLista = (int)self.listaIngredientes.ListaIngredientes.count;
    for (int i = 0; i<logitudaLista; i++) {
        
        Ingrediente * ingrediente =[self.listaIngredientes.ListaIngredientes objectAtIndex:i];
       
        if (i==logitudaLista-1){
             idI = [idI stringByAppendingString:[NSString stringWithFormat:@"%i",ingrediente.idIN]];
        }else {
             idI = [idI stringByAppendingString:[NSString stringWithFormat:@"%i,",ingrediente.idIN]];
        }
        
      
        
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:idI forKey:@"idI"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}


//metodo que cierra el teclado cuando se presiona done 
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)viewWillAppear:(BOOL)animated{
    
    //este metodo se encarga de al abrir la view ver si la hay ingredientes en aceotados en la lista de la compra y si los hay los añade a la despensa o se los suma a los que ya esta a demas de cargar las recetas con estos ingredientes
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"idIC"];
    
    

    if (![savedValue isEqualToString:@""] && savedValue !=nil) {
        
         Ingredientes * addReceta = [[Ingredientes alloc] initIngredientes:2];
        
        if (addReceta.ListaIngredientes) {
            for (Ingrediente * i in addReceta.ListaIngredientes) {
                
                if ([self.listaIngredientes contieneEsteObjeto:i]) {
                    
                    for (Ingrediente *j in self.listaIngredientes.ListaIngredientes) {
                        
                        NSRange r = [j.nombre rangeOfString:i.nombre options:NSCaseInsensitiveSearch];
                        
                        if ( r.location != NSNotFound) {
                            
                            j.cantidad = j.cantidad+i.cantidad;
                        }
                    }
                }else{
                    [self.listaIngredientes addIngredinte:i];
                }
            }
        }
        
       
        NSLog(@"voy a recargarme");
    }
    [self borraIngredientesDespensa];
    [self queIngredientesTenemos];
    [self.tableView reloadData];
}

-(void)borraIngredientesDespensa{
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"idIAborrar"];
    
     if (![savedValue isEqualToString:@""] && savedValue !=nil) {
    
    NSArray *arr = [savedValue componentsSeparatedByString:@","];
    
         for (NSString *i in arr) {
             [self.listaIngredientes borraObjeto:i];
         }
    
    
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"idIAborrar"];
     }
}


@end
