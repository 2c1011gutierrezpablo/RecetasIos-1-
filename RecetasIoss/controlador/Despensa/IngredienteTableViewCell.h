//
//  IngredienteTableViewCell.h
//  RecetasIoss
//
//  Created by Javi on 5/3/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IngredienteTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imagenIng;
@property (weak, nonatomic) IBOutlet UILabel *tituloIng;

@end
