//
//  ApiCodigoDeBarras.m
//  RecetasIoss
//
//  Created by pablo Gutierrez on 21/2/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import "ApiCodigoDeBarras.h"

@implementation ApiCodigoDeBarras

+(NSMutableArray*)consigueIngrediente:(NSString*)codigo{
    
    //en esta clase se haran peticione al servidor de codigos de barras y se devolvera una lista el ingrediente en json
    
    NSError *error;
    NSString *url_string = [NSString stringWithFormat: @"https://world.openfoodfacts.org/api/v0/product/%@.json",codigo];
    NSData *data = [NSData dataWithContentsOfURL: [NSURL URLWithString:url_string]];
    NSMutableArray *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];

    return json;
    
}

@end
