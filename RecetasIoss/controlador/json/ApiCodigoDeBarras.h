//
//  ApiCodigoDeBarras.h
//  RecetasIoss
//
//  Created by pablo Gutierrez on 21/2/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ApiCodigoDeBarras : NSObject

+(NSMutableArray*)consigueIngrediente:(NSString*)codigo;

@end
