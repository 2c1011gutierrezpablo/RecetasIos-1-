//
//  AppDelegate.h
//  RecetasIoss
//
//  Created by pablo Gutierrez on 19/1/18.
//  Copyright © 2018 pablo Gutierrez. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

